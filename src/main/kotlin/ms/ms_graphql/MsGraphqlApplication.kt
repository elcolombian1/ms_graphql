package ms.ms_graphql

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MsGraphqlApplication

fun main(args: Array<String>) {
    runApplication<MsGraphqlApplication>(*args)
}
